from selenium import webdriver
import unittest

from selenium.webdriver.common.keys import Keys


class NewVisitorTest(unittest.TestCase):
    """tests are organized into classes
    inheriting from unittest.TestCase"""

    def setUp(self):
        # runs before each test
        self.browser = webdriver.Chrome()
        # waiting for loading
        self.browser.implicitly_wait(3)

    def tearDown(self):
        # runs after each test
        # closing the browser no matter the result
        self.browser.quit()

    def check_for_row_in_list_table(self, row_text):
        # now the page should list
        # row_text as an item in To-Do list table
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])

    def test_can_start_a_list_and_retrieve_it_later(self):
        """the main body of the test (always starts with /test/)"""

        # check out the 'To-Do' app homepage
        self.browser.get('http://127.0.0.1:8000')

        # check if the page title and header mention to-do lists
        # assertIn(a, b) checks if a in b:
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # enter a To-Do item straight away
        input_box = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            input_box.get_attribute('placeholder'),
            'Enter a to-do item'
        )
        # type in: 'Start reading'
        input_box.send_keys('Start reading')

        # press ENTER
        input_box.send_keys(Keys.ENTER)

        self.check_for_row_in_list_table('1: Start reading')
        self.check_for_row_in_list_table('2: Learn English')
        # there is still an input box inviting
        # to add more items into To-Do list

        # fail no matter what:
        self.fail('Finish the test!')

if __name__ == '__main__':
    # if the test_file has been run, not imported
    # unittest.main() launches all the available tests
    # warnings filters: https://docs.python.org/3/library/warnings.html#warning-filter
    unittest.main(warnings='ignore')
