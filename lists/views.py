from django.shortcuts import render
from lists.models import Item

# Each view is responsible for returning an HttpResponse object.
# https://docs.djangoproject.com/en/1.8/ref/request-response/
# render returns an HttpResponse object


def home_page(request):
    # render searches folders called templates
    # Then it builds an HttpResponse,
    # based on the content of the template.
    if request.method == "POST":
        new_item_text = request.POST['item_text']
        # creating a new Item
        Item.objects.create(text=new_item_text)
    else:
        new_item_text = ''

    # pass the POST parameter to the template
    return render(request, 'home.html', context={
        'new_item_text': new_item_text
    })
