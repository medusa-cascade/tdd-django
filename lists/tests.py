from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase
from django.core.urlresolvers import resolve

from lists.views import home_page
from lists.models import Item


class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        # find view connected to resolving root url
        found = resolve("/")
        # check if this functions is home_page view
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        # use .decode() to convert the response.content bytes into a Python unicode string
        self.assertEqual(response.content.decode(), expected_html)

    def test_home_page_can_save_a_POST_request(self):
        request = HttpRequest()
        request.method = "POST"
        # in templates/home.html > /<input name="item_text"/
        request.POST['item_text'] = 'A new list item'

        response = home_page(request)

        self.assertEqual(Item.objects.count(), 1)  # check that one new item has been saved
        new_item = Item.objects.first()  # .all()[0]
        self.assertEqual(new_item.text, 'A new list item')  # check if item is correct

        # use .decode() to convert the response.content bytes into a Python unicode string
        self.assertIn('A new list item', response.content.decode())
        expected_html = render_to_string(template_name='home.html',
                                         context={'new_item_text': 'A new list item'})
        self.assertEqual(response.content.decode(), expected_html)

    def test_home_page_only_saves_items_when_necessary(self):
        request = HttpRequest()
        home_page(request)
        self.assertEqual(Item.objects.count(), 0)


class ItemModelTest(TestCase):

    def test_saving_and_retrieving_items(self):
        # creating a new record in the database
        first_item = Item()
        first_item.text = 'The first (ever) list item'
        first_item.save()

        # creating another record in the database
        second_item = Item()
        second_item.text = 'Item the second'
        second_item.save()

        # querying the database via a class attribute, .objects
        # the simplest possible query, .all() - returns all items from the table
        saved_items = Item.objects.all()  # returns list-like object called a QuerySet
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]

        self.assertEqual(first_saved_item.text, 'The first (ever) list item')
        self.assertEqual(second_saved_item.text, 'Item the second')

